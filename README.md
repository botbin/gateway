# gateway
gateway is a minimal API gateway that focuses on rate limiting and
strict bearer authentication.

## Usage
### Configuration Files
The service needs configuration files for the HTTP server and APIs
in order to perform routing. Those files can be stored in S3 or
locally.

#### Using S3
There should exist a bucket with a root level `service.toml` file
and a folder named `apis` with any substructure. For example,
```
|- service.toml
|- apis
   |- users
      |- gateway.toml
   |- feeds
      |- gateway.toml
```
The service could then be started using `gateway -s3`

#### Using Local Storage
Assume the following folder layout:
```
|- service.toml
|- apis
   |- users.toml
   |- feeds.toml
```
The service could then be started using: `gateway -config ./service.toml -dir ./apis`

### Environment Variables
- Standard Variables
    - JWT_KEY - used to validate JWTs for bearer authorization
    - GIN_MODE - can be `test` or `release`
- Required if using S3 instead of local storage:
    - AWS_ACCESS_KEY_ID
    - AWS_SECRET_ACCESS_KEY
    - AWS_S3_REGION
    - AWS_S3_BUCKET

## Configuration Formats
The service makes use of two configuration formats: one for the server
and one for the APIs.

### Service Config
An example service config may look like this:
```toml
[server]
port = "8082"
behind_proxy = true
max_body_bytes = 102400
max_header_bytes = 102400
read_timeout = "5s"
write_timeout = "10s"

[ratelimit]
limit = 20
window = "60s"

[authentication]
revoked_tokens_topic = "tokens.revoked"
revoked_tokens_url = "http://localhost:8080/v1/tokens/revoked"

[redis]
host = "localhost"
port = "6379"

[kafka]
brokers = ["localhost:9092"]
```

The notable settings are:
- server
    - behind_proxy - indicates whether the server will be behind a reverse proxy
- authentication
    - revoked_tokens_topic - the Kafka topic where revoked tokens are sent
    - revoked_tokens_url - the URL for fetching the complete list of revoked tokens

### API Config
An example API config may look like this:
```toml
[meta]
name = "Users Service"
owner = "team_blue - tb@company.com"

[api]

    [api.ratelimit]
    limit = 50
    window = "60s"

    [[api.version]]
    name = "v1"
    url = "http://localhost:8081"

        [[api.version.resource]]
        path = "/users"
        methods = [ "GET", "PUT" ]
        protected = true

        [[api.version.resource]]
        path = "/users"
        methods = [ "POST" ]
        protected = false

            [api.version.resource.ratelimit]
            limit = 1
            window = "60s"
```

#### Resource Methods
Resources can supply the list of HTTP methods they will handle. This
allows one to apply different authentication or limit rules to
the same resource. If no methods are specified, the gateway will forward
them all.

#### Protected Resources
Resources can be marked as protected, which means they require a valid
JWT to access. The JWTs should have claims for the subject and expiration
timestamp.

#### Rate Limits
Rate limiting is done with the sliding window method and Redis. An API
may override the service rate limit for the entire spec or for individual
resources. If a limit is applied to an unprotected resource, it is
done through IP-based fingerprinting. Protected resources will rely
on the `sub` claim of validated JWTs.
