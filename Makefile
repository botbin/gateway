PROJECT_NAME := gateway
IMAGE_NAME := ${PROJECT_NAME}

build:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ${PROJECT_NAME} cmd/main.go

# Unit tests only
test:
	go test -coverprofile=cov.out ./pkg/...

# Integration and unit tests
test_all:
	TEST_MODE=all go test -coverprofile=cov.out ./pkg/...

version:
	$(eval VERSION=$(shell ./version.sh))

tag: version
	$(eval LATEST_TAG=v${VERSION})
	git tag -a v${LATEST_TAG} -m v${LATEST_TAG}

image: version
	docker build -f docker/Dockerfile -t ${REGISTRY}/${IMAGE_NAME}:${VERSION} .

deploy_image: image
	docker push ${REGISTRY}/${IMAGE_NAME}:${VERSION}

deploy: tag deploy_image
	git push origin ${LATEST_TAG}
	docker tag ${REGISTRY}/${IMAGE_NAME}:${VERSION} ${REGISTRY}/${IMAGE_NAME}:latest
	docker push ${REGISTRY}/${IMAGE_NAME}:latest

.PHONY: test image