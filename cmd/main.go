package main

import (
	"flag"
	"os"

	"go.uber.org/zap"

	"gitlab.com/botbin/gateway/pkg/config"
	"gitlab.com/botbin/gateway/pkg/web"
)

func init() {
	l, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	zap.ReplaceGlobals(l)
}

const (
	app     = "gateway"
	version = "0.8.0"
)

func main() {
	useS3 := flag.Bool("s3", false, "Prompts the service to load config files from AWS S3")
	apisDir := flag.String("dir", "./apis", "An optional directory from which to load API definitions")
	serviceFP := flag.String("config", "./service.toml", "The location of the service configuration file")
	flag.Parse()

	var serviceDefinition *config.Service
	var apiDefinitions []*config.API

	if *useS3 {
		serviceDefinition, apiDefinitions = loadS3Files()
	} else {
		serviceDefinition, apiDefinitions = loadLocalFiles(*serviceFP, *apisDir)
	}

	zap.S().Infof("%s v%s", app, version)

	err := web.NewServer(serviceDefinition).Start(apiDefinitions)
	if err != nil {
		zap.S().Errorw("failed to start server", "error", err)
	}
}

func loadLocalFiles(serviceFP, apisDir string) (*config.Service, []*config.API) {
	serviceConfig, err := config.NewLocalServiceLoader(serviceFP).Get()
	if err != nil {
		zap.S().Fatal(err)
	}

	apiDefinitions, err := config.NewLocalConfigLoader(apisDir).Get()
	if err != nil {
		zap.S().Errorw("failed to load api definitions", "error", err)
		panic(err)
	}
	return serviceConfig, apiDefinitions
}

func loadS3Files() (*config.Service, []*config.API) {
	region := os.Getenv("AWS_S3_REGION")
	bucket := os.Getenv("AWS_S3_BUCKET")
	store, err := config.NewS3ConfigSource(region, bucket)
	if err != nil {
		zap.S().Fatal(err)
	}

	serviceLoader := store.ServiceLoader("service.toml")
	sdef, err := serviceLoader.Get()
	if err != nil {
		zap.S().Fatal(err)
	}

	apiLoader := store.APILoader("apis")
	adef, err := apiLoader.Get()
	if err != nil {
		zap.S().Errorw("failed to load api definitions", "error", err)
	}

	return sdef, adef
}
