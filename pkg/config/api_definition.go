package config

import (
	"time"

	"github.com/BurntSushi/toml"
)

// API is the top-level definition of an API.
type API struct {
	// Meta specifies details about the API that don't affect its operation.
	Meta Metadata

	// Spec defines how this API operates.
	Spec Specification `toml:"api"`
}

// WriteAt writes the contents of a toml configuration to the api.
// The value for off is not used.
func (api *API) WriteAt(p []byte, off int64) (n int, err error) {
	_, err = toml.Decode(string(p), api)
	return len(p), err
}

// Metadata contains information about an API that does not relate
// to its functional operation.
type Metadata struct {
	// Name describes what the API is meant to do.
	Name string

	// Owner is the person or team that manages the API.
	Owner string
}

// Specification details how an API operates.
type Specification struct {
	// GlobalLimit specifies a global rate limit to apply to resources
	// that do not define their own.
	GlobalLimit RateLimit `toml:"ratelimit"`

	// Versions are API versions for this spec, such as v1 or v2
	Versions []Version `toml:"version"`
}

// RateLimit specifies the rate at which requests can sent to a resource.
type RateLimit struct {
	// Limit specifies how many requests can be made within a window.
	Limit int64

	// Window specifies how long a rate limit window lasts.
	Window Duration
}

// Duration is a time.Duration that can be unmarshaled from text.
type Duration struct {
	time.Duration
}

func (d *Duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))
	return err
}

// Version defines a specific version of an API.
type Version struct {
	// Name is the tag for this version, e.g., v1 or v2
	Name string

	// URL specifies the address where proxied requests should be
	// sent, e.g., http://localhost:8080
	URL string

	// Resources contains the HTTP resources managed by this version.
	Resources []Resource `toml:"resource"`
}

// Resource is a REST resource in an API.
type Resource struct {
	// Path is the resource URI, e.g., /users
	Path string

	// Protected indicates whether the resource employs an authentication
	// mechanism.
	Protected bool

	// LimitOverride specifies a rate limit to override any parent limits.
	LimitOverride RateLimit `toml:"ratelimit"`

	// Methods are the HTTP methods that the resource accepts.
	Methods []string
}
