package config

import "github.com/BurntSushi/toml"

// Service defines how the API gateway should function.
type Service struct {
	Server         Server
	RateLimit      RateLimit `toml:"ratelimit"`
	Authentication Authentication
	Redis          Redis
	Kafka          Kafka
}

// WriteAt writes the contents of a toml configuration to the service.
// The value for off is not used.
func (s *Service) WriteAt(p []byte, off int64) (n int, err error) {
	_, err = toml.Decode(string(p), s)
	return len(p), err
}

// Server specifies how the server should function.
type Server struct {
	// Port defines which port the server will use.
	Port string

	// BehindProxy indicates whether the server is behind a reverse proxy.
	BehindProxy bool `toml:"behind_proxy"`

	// MaxBodyBytes specifies the maximum size of request bodies in bytes.
	MaxBodyBytes int64 `toml:"max_body_bytes"`

	// MaxHeaderBytes specifies the maximum size of request headers in bytes.
	MaxHeaderBytes int64 `toml:"max_header_bytes"`

	// ReadTimeout specifies the read timeout for the server.
	ReadTimeout Duration `toml:"read_timeout"`

	// WriteTimeout specifies the write timeout for the server.
	WriteTimeout Duration `toml:"write_timeout"`
}

// Authentication specifies how the server will handle authentication.
type Authentication struct {
	// RevokedTokensTopic is the Kafka topic where revoked tokens are published.
	RevokedTokensTopic string `toml:"revoked_tokens_topic"`

	// RevokedTokensURL is the URL for fetching the complete list of revoked tokens.
	RevokedTokensURL string `toml:"revoked_tokens_url"`
}

// Redis specifies the Redis connection information for the server.
type Redis struct {
	Host string
	Port string
}

// Kafka specifies the configuration data for the Kafka cluster.
type Kafka struct {
	Brokers []string
}
