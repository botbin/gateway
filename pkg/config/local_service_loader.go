package config

import (
	"github.com/BurntSushi/toml"
)

type localServiceLoader struct {
	fpath string
}

func (lsl localServiceLoader) Get() (*Service, error) {
	conf := &Service{}
	_, err := toml.DecodeFile(lsl.fpath, conf)
	return conf, err
}

// NewLocalServiceLoader creates a ServiceConfigLoader that will
// load a service config from the indicated .toml file.
func NewLocalServiceLoader(fpath string) ServiceConfigLoader {
	return localServiceLoader{fpath: fpath}
}
