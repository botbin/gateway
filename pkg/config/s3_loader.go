package config

import (
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

// S3ConfigSource provides access to config files in AWS S3.
type S3ConfigSource struct {
	bucket     string
	sess       *session.Session
	downloader *s3manager.Downloader
}

// NewS3ConfigSource configures an S3ConfigSource.
// This method will attempt to create an AWS session. Thus, the access
// key id and secret access key should be defined according to the standard
// github.com/aws/aws-sdk-go rules.
func NewS3ConfigSource(region, bucket string) (*S3ConfigSource, error) {
	sess, err := session.NewSession(&aws.Config{Region: &region})
	if err != nil {
		return nil, err
	}

	return &S3ConfigSource{
		bucket:     bucket,
		sess:       sess,
		downloader: s3manager.NewDownloader(sess),
	}, nil
}

// ServiceLoader creates a ServiceConfigLoader that can retrieve a service
// definition from the S3 bucket. Key should be the S3 key for the file.
// E.g., "service.toml"
func (ss *S3ConfigSource) ServiceLoader(key string) ServiceConfigLoader {
	return &s3ServiceLoader{
		key:        &key,
		bucket:     &ss.bucket,
		downloader: ss.downloader,
	}
}

// APILoader creates an APILoader that can retrieve API definitions from
// the S3 bucket. rootDir should be the directory in the bucket where
// the API files are stored. E.g., "apis"
func (ss *S3ConfigSource) APILoader(rootDir string) APILoader {
	return &s3APILoader{
		rootDir:    &rootDir,
		bucket:     &ss.bucket,
		downloader: ss.downloader,
		svc:        s3.New(ss.sess),
	}
}

type s3ServiceLoader struct {
	key        *string
	bucket     *string
	downloader *s3manager.Downloader
}

func (sl *s3ServiceLoader) Get() (*Service, error) {
	serv := new(Service)
	_, err := sl.downloader.Download(serv, &s3.GetObjectInput{
		Bucket: sl.bucket,
		Key:    sl.key,
	})
	return serv, err
}

type s3APILoader struct {
	rootDir    *string
	bucket     *string
	downloader *s3manager.Downloader
	svc        *s3.S3
}

func (sl *s3APILoader) Get() ([]*API, error) {
	objects, err := sl.scanBucket()
	if err != nil {
		return nil, err
	}
	return sl.getAPIs(objects)
}

func (sl *s3APILoader) scanBucket() ([]*s3.Object, error) {
	res, err := sl.svc.ListObjects(&s3.ListObjectsInput{
		Bucket: sl.bucket,
		Prefix: sl.rootDir,
	})

	var items []*s3.Object
	if err == nil {
		items = res.Contents
	}
	return items, err
}

func (sl *s3APILoader) getAPIs(objects []*s3.Object) ([]*API, error) {
	var apis []*API

	for _, obj := range objects {
		if strings.HasSuffix(*obj.Key, ".toml") {
			api, err := sl.makeAPI(obj)
			if err != nil {
				return apis, err
			}
			apis = append(apis, api)
		}
	}

	return apis, nil
}

func (sl *s3APILoader) makeAPI(obj *s3.Object) (*API, error) {
	api := new(API)
	_, err := sl.downloader.Download(api, &s3.GetObjectInput{
		Bucket: sl.bucket,
		Key:    obj.Key,
	})
	return api, err
}
