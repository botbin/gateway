package config

// An APILoader collects API definitions.
type APILoader interface {
	Get() ([]*API, error)
}

// A ServiceConfigLoader loads a service configuration.
type ServiceConfigLoader interface {
	Get() (*Service, error)
}
