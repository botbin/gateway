package config

import (
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/BurntSushi/toml"
	"go.uber.org/zap"
)

type localConfigLoader struct {
	dir string
}

// NewLocalConfigLoader creates a Loader that will load API definitions
// from all .toml files in the given directory.
func NewLocalConfigLoader(directory string) APILoader {
	return localConfigLoader{dir: directory}
}

func (lcl localConfigLoader) Get() ([]*API, error) {
	return lcl.parseConfigFiles()
}

func (lcl localConfigLoader) parseConfigFiles() ([]*API, error) {
	var configs []*API
	pending := &sync.WaitGroup{}
	done, config := make(chan bool), make(chan *API)

	pending.Add(1)
	go lcl.walkDirRecursively(pending, config)

	go func() {
		pending.Wait()
		done <- true
	}()

	for {
		select {
		case <-done:
			return configs, nil

		case api := <-config:
			configs = append(configs, api)
			zap.S().Infow("loaded API",
				"api", api.Meta.Name,
				"owner", api.Meta.Owner,
			)
		}
	}
}

func (lcl localConfigLoader) walkDirRecursively(pending *sync.WaitGroup, config chan<- *API) {
	defer pending.Done()

	filepath.Walk(lcl.dir, func(path string, info os.FileInfo, err error) error {
		if err != nil || info.IsDir() || !strings.HasSuffix(path, ".toml") {
			return nil
		}

		pending.Add(1)
		go lcl.processFile(path, pending, config)
		return nil
	})
}

func (lcl localConfigLoader) processFile(path string, notifier *sync.WaitGroup, config chan<- *API) {
	defer notifier.Done()

	api := &API{}
	if _, err := toml.DecodeFile(path, api); err != nil {
		zap.S().Errorw("failed to decode api definition",
			"error", err,
			"path", path,
		)
	} else {
		config <- api
	}
}
