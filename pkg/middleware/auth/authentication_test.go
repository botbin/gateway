package auth_test

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"gitlab.com/botbin/gateway/pkg/middleware/auth"
	"gitlab.com/botbin/gateway/pkg/token"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

// AuthenticatorTestSuite handles tests for Authenticator.
type AuthenticatorTestSuite struct {
	suite.Suite
	jwtSecret []byte
	handler   http.HandlerFunc
}

func (as *AuthenticatorTestSuite) SetupSuite() {
	token.StartDaemon(&token.DaemonConfig{})
	as.jwtSecret = []byte("test")

	err := os.Setenv("JWT_KEY", string(as.jwtSecret))
	assert.Nil(as.T(), err)

	middl := auth.GetAuthenticator().Get()
	r := gin.New()
	r.Use(middl)
	r.GET("/test", func(ctx *gin.Context) {})
	as.handler = r.ServeHTTP
}

func (as *AuthenticatorTestSuite) TestHandler() {
	tests := []handlerTestCase{
		{
			Token:             as.createJWT(true, false),
			ShouldGrantAccess: false,
		},
		{
			Token:             as.createJWT(false, true),
			ShouldGrantAccess: false,
		},
		{
			Token:             as.createJWT(true, true),
			ShouldGrantAccess: false,
		},
		{
			Token:             as.createJWT(false, false),
			ShouldGrantAccess: true,
		},
	}

	as.runHandlerTests(tests)
}

type handlerTestCase struct {
	Token             string
	ShouldGrantAccess bool
}

func (as *AuthenticatorTestSuite) runHandlerTests(tests []handlerTestCase) {
	for i, test := range tests {
		req := as.getRequest(test.Token)
		rec := httptest.NewRecorder()
		as.handler.ServeHTTP(rec, req)

		if test.ShouldGrantAccess {
			assert.Equal(as.T(), 200, rec.Code, "case %d %+v", i, test)
		} else {
			assert.Equal(as.T(), 401, rec.Code, "case %d", i)
		}
	}
}

func (as *AuthenticatorTestSuite) createJWT(expired, badSignature bool) string {
	var exp int64
	if expired {
		exp = time.Now().Add(-time.Hour).Unix()
	} else {
		exp = time.Now().Add(time.Hour).Unix()
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": int(4),
		"exp": exp,
		"rid": "3",
	})

	var secret []byte
	if badSignature {
		secret = []byte("")
	} else {
		secret = as.jwtSecret
	}

	tokenString, err := token.SignedString(secret)
	assert.Nil(as.T(), err)
	return tokenString
}

func (as *AuthenticatorTestSuite) getRequest(token string) *http.Request {
	req, err := http.NewRequest("GET", "/test", nil)
	assert.Nil(as.T(), err)

	req.Header.Set("Authorization", "Bearer "+token)
	return req
}

func TestAuthenticator(t *testing.T) {
	suite.Run(t, new(AuthenticatorTestSuite))
}
