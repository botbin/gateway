package auth

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"
	"sync"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/gateway/pkg/token"
	"go.uber.org/zap"
)

// Authenticator validates JWTs in request headers.
type Authenticator struct {
	parser    *jwt.Parser
	jwtSecret []byte
	keyFunc   jwt.Keyfunc
}

var (
	authenticator     *Authenticator
	authenticatorOnce sync.Once
)

// GetAuthenticator retrieves a cached authenticator instance.
// This method will panic if it is called for the first time and
// the JWT_KEY environment variable is not set.
func GetAuthenticator() *Authenticator {
	authenticatorOnce.Do(func() {
		jwtKey := []byte(os.Getenv("JWT_KEY"))
		if len(jwtKey) == 0 {
			zap.S().Panicw("missing environment variable JWT_KEY")
		}

		authenticator = newAuthenticator(jwtKey)
	})
	return authenticator
}

func newAuthenticator(jwtKey []byte) *Authenticator {
	return &Authenticator{
		parser:    &jwt.Parser{UseJSONNumber: true},
		jwtSecret: jwtKey,
		keyFunc: func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			return jwtKey, nil
		},
	}
}

// Get returns a handler that prevents unauthenticated requests from proceeding.
//
// It expects each request to contain an Authorization header with a value of
// the format "Bearer the-token". The token must have a value for the "sub" claim.
//
// Tokens are rejected if their signature is invalid, they are expired, or they
// belong to a revoked collection. Successful validations result in the addition
// of the sub claim to the gin context.
func (a *Authenticator) Get() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		sub, err := a.authenticateRequest(ctx.Request)
		if err != nil {
			zap.S().Infow("failed authentication", "error", err, "sub", sub)
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, `{"message": "invalid bearer token"}`)
			return
		}

		ctx.Set("sub", sub)
	}
}

func (a *Authenticator) authenticateRequest(r *http.Request) (int, error) {
	tok, err := a.getBearerToken(r)
	if err != nil {
		return -1, err
	}

	claims, ok := tok.Claims.(jwt.MapClaims)
	if !ok {
		return -1, errors.New("could not parse token claims")
	}

	if token.IsRevoked(claims) {
		return -1, errors.New("token has been revoked")
	}
	return a.getSubject(claims)
}

func (a *Authenticator) getBearerToken(r *http.Request) (*jwt.Token, error) {
	parts := strings.Split(r.Header.Get("Authorization"), " ")
	if len(parts) != 2 || parts[0] != "Bearer" {
		return nil, errors.New("missing bearer token")
	}

	return a.parser.Parse(parts[1], a.keyFunc)
}

// getSubject gets the "sub" claim from the token or returns an error
// if it is not valid.
func (a *Authenticator) getSubject(claims jwt.MapClaims) (int, error) {
	sub, exists := claims["sub"]
	if !exists {
		return -1, errors.New("missing 'sub' claim")
	}

	num, ok := sub.(json.Number)
	if !ok {
		return -1, errors.New("expected 'sub' claim to be an int")
	}

	userID, err := num.Int64()
	return int(userID), err
}
