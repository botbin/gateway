package rate

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// Limiter limits the rate at which a source can send requests.
//
// There are two options for middleware handlers: IP- and token-based limits.
// An API may use both -- but never for the same resource of a given version.
//
// IP limits make a best attempt at fingerprinting the sources of requests
// in order to apply a consistent limit to each source. This can fail if
// the "behind_proxy" service setting is misconfigured.
//
// Token limits require the resource to be protected by JWT authentication.
// Each request will be applied to the "sub" claim of those tokens.
type Limiter struct {
	ltype       LimitType
	keyPrefix   string
	behindProxy bool
	pool        ResourcePool
	writer      *limitWriter
}

// NewLimiter configures a Limiter for use.
func NewLimiter(config LimitConfig) *Limiter {
	keyPrefix := "rl:"
	if config.Namespace != "" {
		keyPrefix += strings.Replace(config.Namespace, " ", ".", -1) + ":"
	}

	if config.Type == 0 {
		config.Type = IP
	}

	return &Limiter{
		ltype:       config.Type,
		keyPrefix:   keyPrefix,
		behindProxy: config.BehindProxy,
		pool:        config.Pool,
		writer:      newWriter(config.Pool.Limit()),
	}
}

// Get creates a middleware that limits the rate at which a client can send
// requests. This may operate on an IP or JWT basis, depending on how the
// limiter was constructed. The following headers are attached to the request:
//
//		X-RateLimit-Limit
//		X-RateLimit-Remaining
//		X-RateLimit-Reset
//
func (l *Limiter) Get() (gin.HandlerFunc, error) {
	switch l.ltype {
	case Token:
		return l.getTokenHandler(), nil

	case IP:
		return l.getIPHandler(), nil

	default:
		return nil, fmt.Errorf("invalid limit type %d", l.ltype)
	}
}

// getTokenHandler gets a middleware function for JWT claims-based rate limiting.
// This relies on a "sub" key in the gin context, which is applied by
// the authorization middleware.
func (l *Limiter) getTokenHandler() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		sub := ctx.MustGet("sub").(int)
		l.limitRequest(ctx, strconv.Itoa(sub))
	}
}

// getIPHandler gets a middleware function for IP rate limiting.
func (l *Limiter) getIPHandler() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		fingerprint := l.getFingerprint(ctx.Request)
		l.limitRequest(ctx, fingerprint)
	}
}

// limitRequest performs the limit function on a request that can
// be traced to a unique entity.
func (l *Limiter) limitRequest(ctx *gin.Context, entityID string) {
	response := l.pool.Request(entityID)

	l.writer.Write(ctx.Writer, response.Remaining, response.NextReset)

	if !response.Fulfilled {
		zap.S().Infow("request rejected due to exceeded rate limit",
			"entityID", entityID,
		)
		ctx.AbortWithStatus(http.StatusTooManyRequests)
	}
}

// getFingerprint attempts to uniquely identify the source of a request.
func (l *Limiter) getFingerprint(r *http.Request) string {
	var fingerprint string
	if l.behindProxy {
		if forwarded := r.Header.Get("X-Forwarded-For"); forwarded != "" {
			// TODO: The client could supply a new list every time, avoiding
			//       fingerprint identification.
			fingerprint = forwarded
		} else if real := r.Header.Get("X-Real-IP"); real != "" {
			fingerprint = real
		} else {
			zap.S().Errorw("proxy proxy failed to supply origin ip; favoring random fingerprint over proxy ip")
			// We don't want to fall back to the proxy ip and rate limit ourselves.
			// We're basically choosing between eventual OOM and self-ban.
			fingerprint = l.randomSig()
		}
	} else {
		fingerprint = r.RemoteAddr
	}
	return base64.StdEncoding.EncodeToString([]byte(fingerprint))
}

func (l *Limiter) randomSig() string {
	base := make([]byte, 16)
	rand.Read(base)
	return string(base)
}
