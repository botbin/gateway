package rate

import (
	"net/http"
	"strconv"
)

// limitWriter writes rate limit headers for HTTP requests.
type limitWriter struct {
	max string
}

func newWriter(maxLimit int64) *limitWriter {
	return &limitWriter{
		max: strconv.FormatInt(maxLimit, 10),
	}
}

// Write sets rate limit response headers in w.
func (lw *limitWriter) Write(w http.ResponseWriter, remaining, reset int64) {
	w.Header().Set("X-RateLimit-Limit", lw.max)
	w.Header().Set("X-RateLimit-Remaining", strconv.FormatInt(remaining, 10))
	w.Header().Set("X-RateLimit-Reset", strconv.FormatInt(reset, 10))
}
