package rate

import (
	"errors"
	"os"
	"testing"
	"time"

	"github.com/satori/go.uuid"

	"github.com/go-redis/redis"
	"gitlab.com/botbin/gateway/pkg/data"
	"gitlab.com/botbin/gateway/pkg/middleware/rate/mocks"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

// Tests cellBasedPool
type CellBasedPoolTestSuite struct {
	suite.Suite
	client *redis.Client
}

func (cs *CellBasedPoolTestSuite) SetupSuite() {
	cs.client = data.GetRedisClient("127.0.0.1:6379")
}

// Ensure that the default burst setting is applied.
func (cs *CellBasedPoolTestSuite) TestNewCellPool_BurstDefault() {
	pool, err := NewCellPool(cs.client, 10, time.Second)
	cs.Nil(err)
	cs.NotEmpty(pool.(*cellBasedPool).burst)
}

// Ensure that Request does not allow more requests than the limit.
func (cs *CellBasedPoolTestSuite) TestRequest_Limit() {
	var limit int64 = 2
	window := time.Second * 10
	pool, err := newCellPoolWithBurst(cs.client, limit, 0, window)
	cs.Nil(err)
	key := uuid.NewV4().String()

	res := pool.Request(key)
	cs.True(res.Fulfilled)

	res = pool.Request(key)
	cs.False(res.Fulfilled)
	cs.Equal(int64(0), res.Remaining)
}

// Ensure that requests go through if Redis is temporarily down.
func (cs *CellBasedPoolTestSuite) TestRequest_RedisUnreachable() {
	var limit int64 = 2
	window := time.Second * 10
	key := uuid.NewV4().String()
	proc := new(mocks.Processor)

	proc.On("Process", mock.Anything).Return(nil).Once()
	pool, err := newCellPoolWithBurst(proc, limit, 0, window)
	cs.Equal(ErrMissingRedisCell, err)
	proc.AssertExpectations(cs.T())

	proc.On("Process", mock.Anything).Return(errors.New("mock error")).Once()
	res := pool.Request(key)
	proc.AssertExpectations(cs.T())

	cs.True(res.Fulfilled)
	cs.Equal(limit, res.Remaining)
}

func TestCellBasedPool(t *testing.T) {
	if os.Getenv("TEST_MODE") == "all" {
		suite.Run(t, new(CellBasedPoolTestSuite))
	}
}
