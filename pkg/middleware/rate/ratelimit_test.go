package rate

import (
	"math"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/suite"
)

// Tests for Limiter
type RateLimiterTestSuite struct {
	suite.Suite
}

// Ensures that the handler returned by Get performs proper rate limiting.
func (rs *RateLimiterTestSuite) TestLimiter_Get() {
	// Define configs here. LimitTypes will be injected for you.
	configs := []LimitConfig{
		{
			Namespace:   "test",
			BehindProxy: false,
		},
		{
			BehindProxy: false,
		},
		{
			Namespace:   "test",
			BehindProxy: true,
		},
		{
			BehindProxy: true,
		},
	}

	limit := rand.Int63n(1000)
	types := []LimitType{IP, Token}

	for _, config := range configs {
		for _, t := range types {
			config.Type = t
			config.Pool = newMockPool(limit)
			rs.runLimiterTest(config, limit)
		}
	}
}

func (rs *RateLimiterTestSuite) runLimiterTest(config LimitConfig, limit int64) {
	limiter := NewLimiter(config)
	handler := rs.makeHandler(limiter, config.BehindProxy)

	for i := int64(0); i < limit; i++ {
		assert.HTTPSuccess(rs.T(), handler.ServeHTTP, "GET", "/test", url.Values{})
	}

	rec := rs.tryLimit(handler)
	rs.assertLimitExceeded(rec, limit)
}

func (rs *RateLimiterTestSuite) makeHandler(limiter *Limiter, fakeProxy bool) http.Handler {
	r := gin.New()
	if fakeProxy {
		ip := "127.0.0." + strconv.Itoa(rand.Intn(128))
		r.Use(func(ctx *gin.Context) {
			ctx.Request.Header.Set("X-Forwarded-For", ip)
		})
	}

	auth := func(ctx *gin.Context) {
		ctx.Set("sub", 3)
	}
	r.Use(auth)

	handler, err := limiter.Get()
	rs.Nil(err)
	r.Use(handler)

	r.GET("/test", func(ctx *gin.Context) {})
	return r
}

func (rs *RateLimiterTestSuite) tryLimit(handler http.Handler) *httptest.ResponseRecorder {
	rec := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/test", nil)
	handler.ServeHTTP(rec, req)
	return rec
}

func (rs *RateLimiterTestSuite) assertLimitExceeded(rec *httptest.ResponseRecorder, limit int64) {
	assert.Equal(rs.T(), 429, rec.Code)
	assert.Equal(rs.T(), "0", rec.Header().Get("X-RateLimit-Remaining"))
	assert.Equal(rs.T(), strconv.FormatInt(limit, 10), rec.Header().Get("X-RateLimit-Limit"))
	assert.NotEmpty(rs.T(), rec.Header().Get("X-RateLimit-Reset"))
}

type mockPool struct {
	lim int64
	cur map[string]int64
}

func newMockPool(limit int64) ResourcePool {
	return &mockPool{
		lim: limit,
		cur: make(map[string]int64),
	}
}

func (mp *mockPool) Request(key string) PoolResponse {
	mp.cur[key]++
	return PoolResponse{
		Remaining: mp.lim - int64(math.Min(float64(mp.lim), float64(mp.cur[key]))),
		NextReset: time.Now().Add(time.Second).Unix(),
		Fulfilled: mp.cur[key] <= mp.lim,
	}
}

func (mp *mockPool) Limit() int64 {
	return mp.lim
}

func TestRateLimiter(t *testing.T) {
	suite.Run(t, new(RateLimiterTestSuite))
}
