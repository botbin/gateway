package rate

import (
	"errors"
	"time"

	"github.com/go-redis/redis"
	"go.uber.org/zap"
)

// A Processor executes general Redis commands.
type Processor interface {
	Process(redis.Cmder) error
}

// cellBasedPool interacts with the redis-cell module in Redis in order
// to perform rate limiting on keys.
//
// Reference: https://github.com/brandur/redis-cell
type cellBasedPool struct {
	proc          Processor
	limit         int64
	burst         int64
	windowSeconds int64
}

// ErrMissingRedisCell indicates that Redis is missing the 'redis-cell' module.
var ErrMissingRedisCell = errors.New(`missing required Redis module 'redis-cell'. Refer to https://github.com/brandur/redis-cell for manual installation. Otherwise, ensure that the Redis image is properly configured`)

// NewCellPool creates a ResourcePool that is based on the generic cell rate algorithm.
func NewCellPool(proc Processor, limit int64, window time.Duration) (ResourcePool, error) {
	return newCellPoolWithBurst(proc, limit, limit/2, window)
}

// This is useful for testing, where we can set burst to 0. In production, you will
// want to use NewCellPool.
func newCellPoolWithBurst(proc Processor, limit, burst int64, window time.Duration) (ResourcePool, error) {
	pool := &cellBasedPool{
		proc:          proc,
		limit:         limit,
		burst:         burst,
		windowSeconds: int64(window.Seconds()),
	}

	if !pool.hasRedisCellModule() {
		return pool, ErrMissingRedisCell
	}
	return pool, nil
}

func (cbp *cellBasedPool) hasRedisCellModule() bool {
	cmd := redis.NewSliceCmd("MODULE", "LIST")
	err := cbp.proc.Process(cmd)
	if err != nil {
		zap.S().Errorw("failed to process MODULE LIST command", "error", err)
		return false
	}

	results, err := cmd.Result()
	if err != nil {
		zap.S().Errorw("MODULE LIST command failed", "error", err)
		return false
	}

	if len(results) == 0 {
		zap.S().Errorw("no Redis modules found")
		return false
	}

	for _, res := range results {
		if cbp.isRedisCell(res) {
			return true
		}
	}
	return false
}

func (cbp *cellBasedPool) isRedisCell(moduleResult interface{}) bool {
	vals, ok := moduleResult.([]interface{})
	if !ok {
		zap.S().Errorw("unexpected module shape; skipping", "res", moduleResult)
		return false
	}

	name, ok := vals[1].(string)
	if !ok {
		zap.S().Errorw("expected name string in Redis module map", "vals", vals)
		return false
	}
	return name == "redis-cell"
}

// Request attempts to acquire a single resource for the given key.
func (cbp *cellBasedPool) Request(key string) PoolResponse {
	cmd := redis.NewSliceCmd("CL.THROTTLE", key, cbp.burst, cbp.limit, cbp.windowSeconds, 1)
	err := cbp.proc.Process(cmd)
	if err != nil {
		return cbp.bail("could not execute Redis throttle command", err)
	}

	res, err := cmd.Result()
	if err != nil {
		return cbp.bail("Redis throttle command failed", err)
	}

	return PoolResponse{
		Fulfilled: res[0].(int64) == 0,
		Remaining: res[2].(int64),
		NextReset: time.Now().UTC().Unix() + res[4].(int64),
	}
}

func (cbp *cellBasedPool) bail(reason string, err error) PoolResponse {
	zap.S().Errorw(reason, "error", err)

	return PoolResponse{
		// It's probably better to let them through while we wait
		// on Redis to come back online.
		Fulfilled: true,
		Remaining: cbp.limit,
		NextReset: time.Now().UTC().Unix(),
	}
}

// Limit specifies how many requests can be made within a window of time.
func (cbp *cellBasedPool) Limit() int64 {
	return cbp.limit
}
