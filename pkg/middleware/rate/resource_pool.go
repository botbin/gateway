package rate

// A ResourcePool represents a pool of scarce resources.
type ResourcePool interface {
	// Request should request a resource for a given key.
	// The key-resource mapping should not change between
	// calls.
	Request(key string) PoolResponse

	// Limit should indicate how many requests can be made
	// within some arbitrary window.
	Limit() int64
}

// PoolResponse specifies the result of requesting a resource from
// a ResourcePool.
type PoolResponse struct {
	// Fulfilled indicates whether the request was satisfied.
	Fulfilled bool

	// Remaining specifies how many more requests can be made.
	Remaining int64

	// NextReset specifies the time (in Unix seconds) when
	// Remaining will equal the value it held prior to the
	// first request.
	NextReset int64
}
