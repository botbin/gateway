package rate

// LimitConfig specifies how RateLimiter should work.
type LimitConfig struct {
	// Namespace creates a scope for all keys.
	// This value is optional.
	Namespace string

	// Pool provides the basis for rate quotas.
	Pool ResourcePool

	// BehindProxy indicates whether the gateway is behind
	// a reverse proxy. The default value is false.
	BehindProxy bool

	// Type specifies how requests are limited.
	// The default value is IP.
	Type LimitType
}

// LimitType defines the datapoint used for rate limiting.
type LimitType int

const (
	// IP suggests rate limiting based on IP addresses.
	IP LimitType = iota + 1
	// Token suggests rate limiting based on a claim from a JWT.
	Token
)