package token

import (
	"sync"

	"github.com/dgrijalva/jwt-go"
	"go.uber.org/zap"
)

var (
	daemon  *tokenDaemon
	starter sync.Once
)

// StartDaemon creates a goroutine that tends to a local cache
// of revoked token references. Those references stay in sync
// with the authorization server, allowing the JWT middleware
// to reject access tokens almost immediately after their parent
// refresh token is revoked.
//
// This method should be called once. Subsequent calls will not
// have any effect.
func StartDaemon(conf *DaemonConfig) (err error) {
	starter.Do(func() {
		daemon = newTokenDaemon(conf)
		if err = daemon.GetGlobalSet(); err != nil {
			return
		}
		err = daemon.Listen()
	})
	return err
}

// IsRevoked determines if the token that held a set of claims has
// been revoked. This method may give false negatives if the revoke
// event has not yet made it through the system. However, one should
// assume the delay to be negligible.
func IsRevoked(tokenClaims jwt.MapClaims) bool {
	rid, ok := tokenClaims["rid"].(string)
	if !ok {
		zap.S().Infow("expected 'rid' claim to be a string")
		return true
	}
	return daemon.Revoked.IsRevoked(rid)
}
