package token

import (
	"encoding/json"
	"errors"
	"net/http"
	"time"
)

// DaemonConfig specifies settings for the token daemon.
type DaemonConfig struct {
	// KafkaBrokers specifies the Kafka broker addresses.
	KafkaBrokers []string

	// RevokedTokensTopic is the topic where revoked tokens are sent.
	RevokedTokensTopic string

	// RevokedTokensAPI is the URL called to retrieve the list
	// of revoked tokens.
	RevokedTokensAPI string
}

type tokenDaemon struct {
	Config  *DaemonConfig
	Revoked *revokedTokenSet
	Client  *http.Client
}

func newTokenDaemon(conf *DaemonConfig) *tokenDaemon {
	return &tokenDaemon{
		Config:  conf,
		Revoked: newRevokedTokenSet(),
		Client: &http.Client{
			Timeout: time.Second * 5,
		},
	}
}

func (td *tokenDaemon) Listen() error {
	source, err := newDefaultTokenStream(td.Config)
	if err != nil {
		return err
	}

	go func() {
		pruneInterval := time.NewTicker(time.Minute)
		for {
			select {
			case <-pruneInterval.C:
				td.Revoked.Prune()

			case tokens := <-source.Tokens():
				td.Revoked.Add(tokens)
			}
		}
	}()
	return nil
}

// TODO: Implement close method.

func (td *tokenDaemon) GetGlobalSet() error {
	references, err := td.requestRevoked()
	if err != nil {
		return err
	}

	td.Revoked.Add(references)
	return nil
}

func (td *tokenDaemon) requestRevoked() ([]revokedToken, error) {
	req, err := http.NewRequest("GET", td.Config.RevokedTokensAPI, nil)
	if err != nil {
		return nil, err
	}

	res, err := td.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, errors.New("expected 200 status code from auth server; got " + res.Status)
	}

	references := []revokedToken{}
	err = json.NewDecoder(res.Body).Decode(&references)
	return references, err
}
