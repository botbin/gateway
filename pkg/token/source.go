package token

import (
	"encoding/json"
	"sync"

	"github.com/Shopify/sarama"
	"go.uber.org/zap"
)

type tokenStream interface {
	Tokens() <-chan []revokedToken
}

type kafkaTokenStream struct {
	tokens     chan []revokedToken
	configurer sync.Once
	consumer   sarama.PartitionConsumer
}

func newDefaultTokenStream(conf *DaemonConfig) (tokenStream, error) {
	sortaConsumer, err := sarama.NewConsumer(conf.KafkaBrokers, nil)
	if err != nil {
		return nil, err
	}

	realziesConsumer, err := sortaConsumer.ConsumePartition(conf.RevokedTokensTopic, 0, sarama.OffsetNewest)
	if err != nil {
		return nil, err
	}

	return &kafkaTokenStream{
		tokens:   make(chan []revokedToken),
		consumer: realziesConsumer,
	}, nil
}

func (kts *kafkaTokenStream) Tokens() <-chan []revokedToken {
	kts.configurer.Do(func() {
		go kts.consumeTopic()
	})
	return kts.tokens
}

func (kts *kafkaTokenStream) consumeTopic() {
	for {
		select {
		case msg := <-kts.consumer.Messages():
			kts.tokens <- kts.convert(msg)

			// TODO: Break if asked to.
		}
	}
}

func (kts *kafkaTokenStream) convert(msg *sarama.ConsumerMessage) []revokedToken {
	tokens := []revokedToken{}
	err := json.Unmarshal(msg.Value, &tokens)
	if err != nil {
		zap.S().Errorw("failed to deserialize revoked token", "consumed_message", msg)
	}
	return tokens
}
