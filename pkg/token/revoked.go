package token

import (
	"sync"
	"time"

	"go.uber.org/zap"
)

// revokedTokenSet is a thread-safe collection of revoked tokens.
type revokedTokenSet struct {
	m      sync.RWMutex
	tokens map[string]*revokedToken
	size   int
}

func newRevokedTokenSet() *revokedTokenSet {
	return &revokedTokenSet{
		tokens: make(map[string]*revokedToken),
	}
}

// IsRevoked returns true if id belongs to a token that has been revoked.
func (rts *revokedTokenSet) IsRevoked(id string) bool {
	rts.m.RLock()
	rts.m.RUnlock()
	return rts.tokens[id] != nil
}

// Add inserts the provided token set into the revoked collection.
func (rts *revokedTokenSet) Add(tokens []revokedToken) {
	rts.m.Lock()
	rts.m.Unlock()

	for i := range tokens {
		rts.tokens[tokens[i].ID] = &tokens[i]
	}
	rts.size += len(tokens)
	zap.S().Infow("added revoked token references", "tokens", tokens)
}

// Prune removes all revoked tokens that have expired.
func (rts *revokedTokenSet) Prune() {
	if rts.size < 10000 { // TODO: Find a good value for this.
		return
	}
	rts.m.Lock()
	rts.m.Unlock()

	now := time.Now().UTC().Unix()
	for id, tok := range rts.tokens {
		if now > tok.ExpiresUnix {
			delete(rts.tokens, id)
			rts.size--
		}
	}
}

// revokedToken identifies an access token that has been revoked.
type revokedToken struct {
	ID          string `json:"rid"`
	ExpiresUnix int64  `json:"expiresAt"`
}
