package proxy

import (
	"strings"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitlab.com/botbin/gateway/pkg/config"
)

// Factory creates HTTP routes based on an API configuration.
type Factory struct {
	service *config.Service
	group   *gin.RouterGroup
}

// NewFactory constructs a default Factory instance.
func NewFactory(service *config.Service, group *gin.RouterGroup) *Factory {
	return &Factory{
		service: service,
		group:   group,
	}
}

// Create makes routes for each version of the API.
func (f *Factory) Create(api *config.API) error {
	zap.S().Infow("creating routes", "api", api.Meta.Name)
	defer zap.S().Infow("finished creating routes", "api", api.Meta.Name)

	for _, version := range api.Spec.Versions {
		err := f.createRoutesForVersion(api, &version)
		if err != nil {
			return err
		}
	}
	return nil
}

func (f *Factory) createRoutesForVersion(api *config.API, v *config.Version) error {
	for _, resource := range v.Resources {
		uri := f.getURI(v.Name, resource.Path)
		builder := f.createTemplate(api, uri, v.URL)

		if err := f.createRoute(uri, &resource, builder); err != nil {
			return err
		}
	}
	return nil
}

func (f *Factory) createTemplate(api *config.API, uri, proxyURL string) *RouteBuilder {
	grp := f.group.Group("/")
	builder := NewRouteBuilder(grp, proxyURL, uri)

	limit := f.service.RateLimit
	if limit.Limit != 0 {
		builder = builder.WithLimit("global", limit.Limit, limit.Window.Duration, f.service.Server.BehindProxy)
	}

	limit = api.Spec.GlobalLimit
	if limit.Limit != 0 {
		builder = builder.WithLimit(api.Meta.Name, limit.Limit, limit.Window.Duration, f.service.Server.BehindProxy)
	}
	return builder
}

func (f *Factory) getURI(versionName, resourcePath string) string {
	return "/" + versionName + resourcePath
}

func (f *Factory) createRoute(uri string, resource *config.Resource, builder *RouteBuilder) error {
	if resource.Protected {
		builder = builder.WithAuth()
	}

	limit := resource.LimitOverride
	if limit.Limit != 0 {
		key := uri + strings.Join(resource.Methods, "+")
		builder = builder.WithLimit(key, limit.Limit, limit.Window.Duration, f.service.Server.BehindProxy)
	}

	if len(resource.Methods) != 0 {
		builder = builder.WithMethods(resource.Methods)
	}

	return builder.Build()
}
