package proxy

import (
	"errors"
	"net/http/httputil"
	"net/url"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/gateway/pkg/data"
	"gitlab.com/botbin/gateway/pkg/middleware/auth"
	"gitlab.com/botbin/gateway/pkg/middleware/rate"
)

// RouteBuilder builds routes into gin router groups.
type RouteBuilder struct {
	proxyURL             string
	uri                  string
	methods              []string
	needsAuth            bool
	needsLimit           bool
	limit                int64
	limitWindow          time.Duration
	limitKey             string
	behindProxy          bool
	isMiddlewareAttached bool
	group                *gin.RouterGroup
}

// NewRouteBuilder creates a RouteBuilder with the minimum required values.
func NewRouteBuilder(grp *gin.RouterGroup, proxyURL, uri string) *RouteBuilder {
	return &RouteBuilder{
		proxyURL: proxyURL,
		uri:      uri,
		group:    grp,
	}
}

// WithMethods specifies which HTTP methods the route will handle.
// If called multiple times, the methods will be appended to the
// running list.
func (rb *RouteBuilder) WithMethods(methods []string) *RouteBuilder {
	rb.methods = append(rb.methods, methods...)
	return rb
}

// WithAuth enables authentication middleware.
func (rb *RouteBuilder) WithAuth() *RouteBuilder {
	rb.needsAuth = true
	return rb
}

// WithLimit enables rate limiting middleware.
func (rb *RouteBuilder) WithLimit(key string, limit int64, window time.Duration, behindProxy bool) *RouteBuilder {
	rb.needsLimit = true
	rb.limit = limit
	rb.limitWindow = window
	rb.limitKey = key
	rb.behindProxy = behindProxy
	return rb
}

// Build applies the specified route configuration to the router group.
func (rb *RouteBuilder) Build() error {
	if err := rb.attachMiddleware(); err != nil {
		return err
	}
	return rb.attachProxy()
}

// attachMiddleware applies pre-proxy middleware to the route.
func (rb *RouteBuilder) attachMiddleware() error {
	middlewareHandlers, err := rb.createMiddleware()
	if err != nil {
		return err
	}

	for _, m := range middlewareHandlers {
		rb.group.Use(m)
	}
	rb.isMiddlewareAttached = true
	return nil
}

// createMiddleware creates handlers that will process requests before they
// are sent through the reverse proxy.
func (rb *RouteBuilder) createMiddleware() ([]gin.HandlerFunc, error) {
	var handlers []gin.HandlerFunc

	if rb.needsAuth {
		auth := auth.GetAuthenticator().Get()
		handlers = append(handlers, auth)
	}

	if rb.needsLimit {
		if handler, err := rb.createRateLimitHandler(); err != nil {
			return handlers, err
		} else {
			handlers = append(handlers, handler)
		}
	}
	return handlers, nil
}

// createRateLimitHandler creates a handler that will perform rate limiting.
func (rb *RouteBuilder) createRateLimitHandler() (gin.HandlerFunc, error) {
	ltype := rate.Token
	if !rb.needsAuth {
		ltype = rate.IP
	}

	pool, err := rate.NewCellPool(data.GetRedisClient(), rb.limit, rb.limitWindow)
	if err != nil {
		return nil, err
	}

	limiter := rate.NewLimiter(rate.LimitConfig{
		Namespace:   rb.limitKey,
		BehindProxy: rb.behindProxy,
		Type:        ltype,
		Pool:        pool,
	})
	return limiter.Get()
}

// attachProxy applies a reverse proxy to the route.
func (rb *RouteBuilder) attachProxy() error {
	if !rb.isMiddlewareAttached {
		return errors.New("middleware should be attached before the proxy")
	}

	proxy, err := rb.createProxy()
	if err != nil {
		return err
	}

	if len(rb.methods) != 0 {
		for _, m := range rb.methods {
			rb.group.Handle(m, rb.uri, proxy)
		}
	} else {
		rb.group.Any(rb.uri, proxy)
	}
	return nil
}

// createProxy creates handler that acts as a reverse proxy.
func (rb *RouteBuilder) createProxy() (gin.HandlerFunc, error) {
	destination, err := url.Parse(rb.proxyURL)
	if err != nil {
		return nil, err
	}

	proxy := httputil.NewSingleHostReverseProxy(destination)
	return func(ctx *gin.Context) {
		proxy.ServeHTTP(ctx.Writer, ctx.Request)
	}, nil
}
