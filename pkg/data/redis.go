package data

import (
	"sync"

	"github.com/go-redis/redis"
	"go.uber.org/zap"
)

var (
	redisClient *redis.Client
	rdOnce      sync.Once
)

// GetRedisClient retrieves a cached Redis client.
// Only the first call must specify an address. That address will then be
// used for all subsequent calls, even if they provide arguments.
func GetRedisClient(addr ...string) *redis.Client {
	rdOnce.Do(func() {
		redisClient = redis.NewClient(&redis.Options{
			Addr: addr[0],
			MaxRetries: 5,
		})

		_, err := redisClient.Ping().Result()
		if err != nil {
			zap.S().Errorw("Redis did not respond to ping", "error", err)
		}
	})
	return redisClient
}
