package web

import (
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/gateway/pkg/config"
	"gitlab.com/botbin/gateway/pkg/data"
	"gitlab.com/botbin/gateway/pkg/proxy"
	"gitlab.com/botbin/gateway/pkg/token"
	"go.uber.org/zap"
)

// Server encapsulates the HTTP server for the service.
type Server struct {
	srv  *server
	conf *config.Service
}

// NewServer configures a non-running Server according to a configuration.
func NewServer(conf *config.Service) *Server {
	data.GetRedisClient(conf.Redis.Host + ":" + conf.Redis.Port)
	return &Server{
		srv: &server{
			OMaxBodyBytes:   conf.Server.MaxBodyBytes,
			OMaxHeaderBytes: conf.Server.MaxHeaderBytes,
			OReadTimeout:    conf.Server.ReadTimeout,
			OWriteTimeout:   conf.Server.WriteTimeout,
		},
		conf: conf,
	}
}

// Start creates a running HTTP server that proxies requests to the
// given APIs.
func (s *Server) Start(apis []*config.API) error {
	if err := s.configureTokenDaemon(); err != nil {
		return err
	}

	router, err := s.configureRouter(apis)
	if err != nil {
		return err
	}

	zap.S().Infow("server started", "port", s.conf.Server.Port)
	return s.srv.Start(":"+s.conf.Server.Port, router)
}

func (s *Server) configureTokenDaemon() error {
	return token.StartDaemon(&token.DaemonConfig{
		KafkaBrokers:       s.conf.Kafka.Brokers,
		RevokedTokensTopic: s.conf.Authentication.RevokedTokensTopic,
		RevokedTokensAPI:   s.conf.Authentication.RevokedTokensURL,
	})
}

func (s *Server) configureRouter(apis []*config.API) (*gin.Engine, error) {
	router := gin.New()
	router.Use(gin.Recovery())
	router.Use(s.getCors())

	return router, s.addProxies(router, apis)
}

func (s *Server) getCors() gin.HandlerFunc {
	return cors.New(cors.Config{
		AllowOrigins:     []string{"http://dev.botbin.io", "https://botbin.io"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		AllowMethods:     []string{"GET", "POST", "PUT", "HEAD"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	})
}

func (s *Server) addProxies(router *gin.Engine, apis []*config.API) error {
	factory := proxy.NewFactory(s.conf, router.Group("/"))

	for _, api := range apis {
		err := factory.Create(api)
		if err != nil {
			return err
		}
	}
	return nil
}
