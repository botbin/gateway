package web

import (
	"net/http"

	"gitlab.com/botbin/gateway/pkg/config"
)

// server encapsulates a secure HTTP server with sensible limits on request
// size and read/write durations.
type server struct {
	http.Server
	OMaxBodyBytes   int64
	OMaxHeaderBytes int64
	OReadTimeout    config.Duration
	OWriteTimeout   config.Duration
}

// Start opens the server to requests at a specific port with a root handler.
// This method will block the thread in which it is called.
func (s *server) Start(port string, handler http.Handler) error {
	s.Addr = port
	s.Handler = reqBodyLimiter{handler: handler, sizeBytes: s.OMaxBodyBytes}
	s.MaxHeaderBytes = int(s.OMaxHeaderBytes)
	s.ReadTimeout = s.OReadTimeout.Duration
	s.WriteTimeout = s.OWriteTimeout.Duration
	return s.ListenAndServe()
}

type reqBodyLimiter struct {
	handler   http.Handler
	sizeBytes int64
}

// ServeHTTP limits the size of the request body before delegating the request
// to the main handler.
func (rbl reqBodyLimiter) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	r.Body = http.MaxBytesReader(w, r.Body, rbl.sizeBytes)
	rbl.handler.ServeHTTP(w, r)
}
